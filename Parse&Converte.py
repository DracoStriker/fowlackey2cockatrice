import re


# TODO Token cards
class FowCard:

    def __init__(self, argv):
        print(argv[0])
        self.name = argv[0]
        self.set = argv[1]
        self.set_code = argv[2]
        self.type = argv[3]
        self.attribute = argv[4]
        self.rarity = argv[5]
        self.cost = argv[6]
        self.total_cost = argv[7]
        self.race = argv[8]
        self.trait = argv[9]
        self.attack = argv[10]
        self.defense = argv[11]
        # self.divinity = argv[12]
        if len(argv) > 13:
            self.text = argv[13]
        else:
            self.text = ''
        self.j_ruler = []
        self.inverse = []
        self.shift = []

        # -- post processing --

        # name
        stranger = self.name.endswith('(Stranger)')
        self.name = re.sub(' \(.*?\)', '', self.name)
        if stranger:
            self.name += ' (Stranger)'
        if self.type == 'J-Ruler':
            if self.name == 'The Time Spinning Witch' or self.name == 'Reiya, Fourth Daughter of the Mikage' \
                    or self.name == 'Treasure Hunter Fierica' or self.name == 'Martial Artist Pialle Eille' \
                    or self.name == 'Mephina, Mermaid Shaman' or self.name == 'Leaf Elder' \
                    or self.name == 'Bloodsucking Butler' or self.name == 'Blazer Gill Rabus' \
                    or self.name == 'Hannibal Barca' or self.name == 'Alexander' or self.name == 'Julius Caesar' \
                    or self.name == 'Minamoto no Yoshitsune' or self.name == 'Vlad Tepes':
                self.name += ' [J-Ruler]'
        self.name = self.name.replace('[J-ruler]', '[J-Ruler]').replace('&', '&amp;')
        if len(self.name) > 0 and self.name[0] == '"':
            self.name = self.name[1:-1].replace('""', '"')

        # text
        self.text = self.text[:-1].replace('</br>', '\n').replace('<br.>', '\n').replace('<br/>', '\n') \
            .replace('<', '&lt;').replace('>', '&gt;')
        if len(self.text) > 0 and self.text[0] == '"':
            self.text = self.text[1:-1].replace('""', '"')

        # set_code
        self.set_code = self.set_code.split(',')[0]

        # cost
        self.cost = self.cost.replace('{', '').replace(' ', '').replace('}', '')
        if len(self.cost) > 1 and self.cost[-1].isnumeric():
            self.cost = self.cost[-1] + self.cost[:-1]

        # total_cost
        if self.cost == '':
            self.total_cost = '0'

    def to_xml(self):
        return '\n\t<card>\n' \
               '\t  <name>%s</name>\n' \
               '\t  <text>%s</text>\n' \
               '\t  <prop>\n' \
               '\t    <layout>%s</layout>\n' \
               '\t    <side>%s</side>\n' \
               '\t    <type>%s</type>\n' \
               '\t    <maintype>%s</maintype>\n' \
               '\t    <manacost>%s</manacost>\n' \
               '\t    <cmc>%s</cmc>\n' \
               '%s%s' \
               '\t  </prop>\n' \
               '\t  <set %spicurl="https://fowlibrary.com/lackey/sets/%s/%s.jpg">%s</set>\n' \
               '%s%s%s%s' \
               '\t  <tablerow>%s</tablerow>\n' \
               '\t</card>' % (
                   self.name, self.text, self._layout(), self._side(), self._type(), self._main_type(), self.cost,
                   self.total_cost, self._colors(), self._pt(), self._rarity(), self.set, self.set_code,
                   self.set.replace('SDA1', 'SDAO1').replace('SDA2', 'SDAO2'), self._j_ruler(), self._inverse(),
                   self._shift(), self._token(), self._table_row())

    def _layout(self):
        return 'transform' if (self.trait.find('Inverse') != -1 or self.race.find('Inverse') != -1 or
                               self.text.find('nvert this card.') != -1 or (
                                       (self.type == 'Ruler' or self.type == 'J-Ruler') and
                                       self.name != 'Grimm, the Fairy Tale Prince' and self.name
                                       != 'Yggdrasil, the World Tree' and self.name
                                       != 'Yggdrasil, Malefic Verdant Tree')) else 'normal'

    def _side(self):
        return 'back' if self.type == 'J-Ruler' or self.trait.find('Inverse') != -1 or self.race.find('Inverse') != -1 \
            else 'front'

    def _type(self):
        main_type = self._main_type()
        _type = (main_type if main_type != 'Magic Stone' else self.type) + (
            ' (%s)' % self.trait if self.trait != '' else '') + (
                    ((' (Stranger)' if self.name.endswith(
                        '(Stranger)') else '') + (' (Shift)' if len(self.shift) > 0 and self.text.find(
                        '[Shift]') == -1 else '') + ': %s' % self.race) if self.race != '' else '')
        return _type if _type != 'True Magic Stone' else 'Special Magic Stone/True Magic Stone'

    def _main_type(self):
        return self.type.split(' ')[0].replace(':', '').replace('Magic', 'Magic Stone') \
            .replace('Basic', 'Magic Stone').replace('Special', 'Magic Stone').replace('True', 'Magic Stone')

    def _colors(self):
        if self.attribute == 'Void' or self._main_type() == 'Basic Magic Stone' or \
                self._main_type() == 'Special Magic Stone':
            return ''
        c = ''
        for s in self.attribute.split('/'):
            c += 'W' if s == 'Light' else 'R' if s == 'Fire' else 'U' if s == 'Water' else 'G' if s == 'Wind' else 'B'
        return '\t    <colors>%s</colors>\n' % c

    def _pt(self):
        return '' if self.attack == '' else '\t    <pt>%s/%s</pt>\n' % (self.attack, self.defense)

    def _rarity(self):
        if self.rarity == '':
            return ''
        r = self.rarity
        r = 'common' if r == 'Normal' else 'common' if r == 'Common' else 'uncommon' if r == 'Uncommon' else 'rare' \
            if r == 'Rare' else 'super rare'
        return 'rarity="%s" ' % r

    def _token(self):
        return '\t  <token>1</token>\n' if self.type == 'Token' else ''

    def _table_row(self):
        if __name__ == '__main__':
            return '0' if self.type.find('Magic Stone') != -1 else '2' if (self._main_type() == 'Resonator' or
                                                                           self._main_type() == 'J-Ruler') else '3' \
                if self._main_type() == 'Chant' else '1'

    def _j_ruler(self):
        related = ''
        for j_ruler in self.j_ruler:
            related += '\t  <related attach="attach">%s</related>\n' % j_ruler
        return related

    def _inverse(self):
        related = ''
        for inverse in self.inverse:
            related += '\t  <related attach="attach">%s</related>\n' % inverse
        return related

    def _shift(self):
        related = ''
        for shift in self.shift:
            related += '\t  <related attach="attach">%s</related>\n' % shift
        return related


def main():
    c_j_ruler = None
    c_inverse = None
    c_colossal = None
    c_shift = None
    with open("carddata.txt", "r") as f:
        with open("FoW.xml", "a") as db:
            f.readline()
            line = f.readline()
            while line:
                c = FowCard(line.split('\t'))
                if c.type == 'Ruler':
                    c_j_ruler = FowCard(f.readline().split('\t'))
                    c.j_ruler += [c_j_ruler.name]
                if c.text.find('nvert this card.') != -1:
                    c_inverse = FowCard(f.readline().split('\t'))
                    c.inverse += [c_inverse.name]
                if c.text.find('[Shift]') != -1:
                    c_shift = FowCard(f.readline().split('\t'))
                    c.shift += [c_shift.name]
                    c_shift.shift = [c.name]
                db.write(c.to_xml())
                if c_j_ruler is not None:
                    if c_j_ruler.name == 'Ebon Dragon Emperor, Gill Alhama\'at' or \
                            c_j_ruler.name == 'The Time Spinning Witch [J-Ruler]':
                        c_colossal = FowCard(f.readline().split('\t'))
                        c_j_ruler.j_ruler += [c_colossal.name]
                    db.write(c_j_ruler.to_xml())
                    c_j_ruler = None
                    if c_colossal is not None:
                        db.write(c_colossal.to_xml())
                        c_colossal = None
                if c_inverse is not None:
                    db.write(c_inverse.to_xml())
                    c_inverse = None
                if c_shift is not None:
                    db.write(c_shift.to_xml())
                    c_shift = None
                line = f.readline()
            db.write('\n  </cards>\n</cockatrice_carddatabase>')


if __name__ == "__main__":
    main()
